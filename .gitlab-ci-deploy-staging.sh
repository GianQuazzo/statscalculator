#!/bin/bash

set -xe

# Connect to the staging VM through SSH and execute the following steps:
# - login into the GitLab Container Registry
# - remove a previous running container (if exists)
# - pull the latest image
# - run the app container
# - exit
ssh -o PreferredAuthentications=publickey $STAGING_USER@$STAGING_ADDRESS \
"docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY \
&& docker rm --force statscalculator || true \
&& docker pull $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME \
&& docker run -d --restart on-failure:5 --name statscalculator -p $STAGING_PORT:8080 $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME \
&& exit"