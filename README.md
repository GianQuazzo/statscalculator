# StatsCalculator

## Membri del gruppo
* Gianluca Quaglia 829533
* Michele Rago 830616

## Descrizione
Statscalculator è un'applicazione in grado di ricevere un dataset di valori numerici interi composto da due campioni di popolazioni e restituire i valori delle seguenti statistiche campionarie: 
* Media campionare per ciascun campione
* Varianza campionaria per ciascun campione
* Deviazione standard campionaria per cascun campione
* Covarianza campionaria
* Indice di correlazione di Pearson

Il dataset viene fornito all'applicazione tramite API rispettando il seguente formato: {x: [x_1, ..., x_n], y: [y_1, ..., y_n], dim: n}, mentre la risposta con le informazioni da essa elaborata verranno restituite nel formato : {avX: \<value\>,avY: \<value\>,varX: \<value\>,varY: \<value\>,sdX: \<value\>,sdY: \<value\>,covXY: \<value\>,pearsIndex: \<value\>}.
 
## Sviluppo
L'applicazione è stata sviluppata in linguaggio Java e la responsabilità è stata suddivisa tra le varie classi del progetto nel seguente modo: 
* Dataset.java     ->   classe che delinea il formato in input dei dati che riceverà il calcolatore
* Output.java      ->   classe che delinea il formato di output dei dati che restituirà il calcolatore
* Calculator.java  ->   classe che riceve in input un dataset ed esegue i calcoli necessari per restituire le statistiche sopra elencate; queste ultime vengono restituite
sottoforma di output descritto dalla classe Output.java
* Server.java      ->   classe che si occupa di ricevere richieste http e di restituirne il risultato in formato JSON

## Dipendenze
Per il parsing dei file JSON ricevuti e restituiti dalla classe Server quest'ultima fa utilizzo della libreria Gson (https://github.com/google/gson/blob/master/UserGuide.md).

## PIPELINE
La pipeline sviluppata per il progetto appena descritto prevede i seguenti stage:
* build
* verify
* unit-test
* integration-test
* package
* release
* deploy

Di seguito vengono vengono esplicate nello specifico le azioni intraprese per la costruzione degli stage sopra elencati.

### Build
Viene compilato il codice e vengono scaricate tutte le dipendenze necessarie utilizzando il compilatore maven.

### Verify
Lo stage è stato suddiviso in due sottofasi: lint e stats.

#### Lint
In questa fase viene utilizzato il plugin maven checkstyle il quale permette di eseguire una verifica e dunque la generazione di un rapport sullo stile del codice sorgente; a tal proposito è stato scelto di sopprimere alcuni controlli riguardanti i seguenti parametri di stile: controlli relativi alla Javadoc, controlli relativi al massimo numero di parametri per un singolo metodo, controlli relativi ad HiddensFields (https://checkstyle.sourceforge.io/apidocs/com/puppycrawl/tools/checkstyle/checks/coding/HiddenFieldCheck.html)

Tali eccezioni sono state inserite all'interno del file checkstyle-suppressions.xml.

#### Stats
Viene eseguito un controllo del codice tramite SpotBugs per ricercare eventuali bug nel codice sorgente.

### Unit-test
In questa fase vengono eseguiti i test unitari Junit volti a verificare la correttezza del funzionamento delle singole componenti del progetto; in questo caso vi è un solo test il quale ha come scopo quello ti verificare la correttezza dei calcoli eseguiti dalla classe Calculator.java, per lo specifico del metodo pubblico Run(); il test viene eseguito utilizzando il comando mvn test; la separazione tra unit test e integretion-test è resa possibile dalle specifiche riguardanti il plugin surfire inserite nel file pom.xml.

### Integration-test
In questa fase vengono eseguiti i test di integrazione volti a verificare la correttezza del funzionamento dell'intero sistema; in questo caso verrà eseguito un solo test il quale ha come scopo quello di verificare il corretto funzionamento della chiamata all' endpoint dell'applicazione (messa a disposizione dalla classe Server.java): viene quindi effettuata una chiamata a quest'ultimo e ne viene analizzata la risposta. 

### Package
Viene creato il file .jar con le relative dipendenze attraverso il comando mvn clean package.

### Release
In questa fase verrà costruita un immagine docker contente il pacchetto prodotto nella fase di package e verrà caricata sul Gitlab Container Registry.

### Deploy
In questa fase ci si collega ad un macchina virtuale debian 10 tramite ssh e si sotituirà il docker container presente di StatsCalculator con l'ultima versione presente nel Gitlab Container Registry.

### Nota sulle variabili d'ambiente
Alcune delle variabili utilizzate all'interno della pipelpine non sono state definite all'interno del file stesso ma bensì
sono state dichiarate come variabili nascoste all'interno delle impostazione della repository. Nel particolare queste variabili sono:

* SSH_PRIVATE_KEY : chiave per l'autenticazione via ssh alla macchina virtuale utilizzata in fase di deploy
* STAGING_ADDRESS : indirizzo della macchina virtuale
* STAGING_PORT : porta utilizzata dal servizio ssh della macchina virtuale
* STAGING_USER : nome utente della macchina virtuale