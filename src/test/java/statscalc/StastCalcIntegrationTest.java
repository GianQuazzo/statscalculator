package statscalc;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

import com.google.gson.Gson;


public class StastCalcIntegrationTest {

	
	@Test
	public void IntegrationTest() {
		Server.run();
		
		String endpoint = "http://127.0.0.1:8080/statsCalculator";
		String message = "{\"x\":[3,4,1,5,8,2],\"y\":[5,3,3,1,8,3],\"dim\":6}";
		
		
		try {
			URL url = new URL(endpoint);
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)con;
			http.setRequestMethod("POST");
			http.setDoOutput(true);
			
			byte[] out = message.getBytes(StandardCharsets.UTF_8);
			int length = out.length;

			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			http.connect();
			try(OutputStream os = http.getOutputStream()) {
			    os.write(out);
			}
			
			
			String result = "";
			BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) 
				result += inputLine;
			in.close();
			Server.stop();
			
			Output output = new Gson().fromJson(result, Output.class);
			
			assertEquals(output.getAvX(), 3.8333, 0.0001);
			assertEquals(output.getAvY(), 3.8333, 0.0001);
			assertEquals(output.getVarX(), 6.1667, 0.0001);
			assertEquals(output.getVarY(), 5.7667, 0.0001);
			assertEquals(output.getSdX(), 2.4833, 0.0001);
			assertEquals(output.getSdY(), 2.4014, 0.0001);
			assertEquals(output.getCovXY(), 3.3667, 0.0001);
			assertEquals(output.getPearsIndex(), 0.5646, 0.0001);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
