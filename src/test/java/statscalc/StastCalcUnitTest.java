package statscalc;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;


public class StastCalcUnitTest {

	@Test
	public void RunTest() {
		
		
		List<Integer> x = Arrays.asList(3, 4, 1, 5, 8, 2);
		List<Integer> y = Arrays.asList(5, 3, 3, 1, 8, 3);
		
		Dataset input = new Dataset(x, y);
		Calculator calc = new Calculator(input);
		Output out = calc.run();
		
		assertEquals(out.getAvX(), 3.8333, 0.0001);
		assertEquals(out.getAvY(), 3.8333, 0.0001);
		assertEquals(out.getVarX(), 6.1667, 0.0001);
		assertEquals(out.getVarY(), 5.7667, 0.0001);
		assertEquals(out.getSdX(), 2.4833, 0.0001);
		assertEquals(out.getSdY(), 2.4014, 0.0001);
		assertEquals(out.getCovXY(), 3.3667, 0.0001);
		assertEquals(out.getPearsIndex(), 0.5646, 0.0001);		
	}
		
}
