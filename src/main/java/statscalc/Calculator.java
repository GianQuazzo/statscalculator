package statscalc;

import java.util.List;

public final class Calculator {
    private final Dataset data;

    public Calculator(final Dataset data) {
        this.data = data;
    }

    public Output run() {
        double avX, avY, varX, varY, sdX, sdY, covXY, pearsIndex;
        avX = avarage(data.getX());
        avY = avarage(data.getY());
        varX = variance(data.getX(), avX);
        varY = variance(data.getY(), avY);
        sdX = standardDeviation(varX);
        sdY = standardDeviation(varY);
        covXY = covariance(data.getX(), data.getY(), avX, avY);
        pearsIndex = pearsonIndex(covXY, varX, varY);
        return new Output(avX, avY, varX, varY, sdX, sdY, covXY, pearsIndex);
    }


    private double avarage(final List<Integer> l) {
        double s = 0;
        for (int i = 0; i < data.getDim(); i++) {
            s += l.get(i);
        }
        return s / data.getDim();
    }

    private double variance(final List<Integer> l, final double avarage) {
        double s = 0;
        for (int i = 0; i < data.getDim(); i++) {
            s += Math.pow((l.get(i) - avarage), 2);
        }
        return s / (data.getDim() - 1);
    }

    private double standardDeviation(final double variance) {
        return Math.sqrt(variance);
    }

    private double covariance(
            final List<Integer> x,
            final List<Integer> y,
            final double avarX,
            final double avarY) {
        double s = 0;
        for (int i = 0; i < data.getDim(); i++) {
            s += (x.get(i) - avarX) * (y.get(i) - avarY);
        }
        return s / (data.getDim() - 1);
    }

    private double pearsonIndex(
            final double covariance,
            final double varX,
            final double varY) {
        return (covariance) / Math.sqrt(varX * varY);
    }
}
