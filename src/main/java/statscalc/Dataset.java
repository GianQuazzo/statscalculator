package statscalc;

import java.util.List;

public final class Dataset {
    private final List<Integer> x;
    private final List<Integer> y;
    private final int dim;

    public Dataset(final List<Integer> x, final List<Integer> y) {
        super();
        this.x = x;
        this.y = y;
        this.dim = x.size();
    }

    public List<Integer> getX() {
        return x;
    }
    public List<Integer> getY() {
        return y;
    }
    public int getDim() {
        return dim;
    }
}
