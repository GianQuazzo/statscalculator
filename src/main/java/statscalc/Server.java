package statscalc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public final class Server {
    private static HttpServer httpServer;
    private static final int SERVER_PORT = 8080;
    private static final int RESPONSE_HEADERS_CODE = 200;

    private Server() { }

    public static void main(final String[] args) {
        run();
    }

    public static void run() {
        try {
            httpServer =
                    HttpServer.create(new InetSocketAddress(SERVER_PORT), 0);
            httpServer.createContext("/statsCalculator", new ServerHandler());
            httpServer.start();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void stop() {
        httpServer.stop(0);
    }

    // Handler for '/statsCalculator' context
    static class ServerHandler implements HttpHandler {

        @Override
        public void handle(final HttpExchange he) throws IOException {


            // Serve for POST requests only
            if (
                he.getRequestMethod().equals("Post")
                || he.getRequestMethod().equals("POST")
                || he.getRequestMethod().equals("post")) {
                try {
                    System.out.println("Serving the request");
                    // REQUEST Headers
                    Headers requestHeaders = he.getRequestHeaders();

                    int contentLength
                        = Integer.parseInt(
                                requestHeaders.getFirst("Content-length"));

                    // REQUEST Body
                    InputStream is = he.getRequestBody();
                    byte[] data = new byte[contentLength];
                    is.read(data);

                    handleResponse(he, calculate(new String(data)));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        private String calculate(final String data) {
            Gson gson = new Gson();
            Dataset dataset = gson.fromJson(new String(data), Dataset.class);

            Calculator calc = new Calculator(dataset);
            Output out = calc.run();

            return gson.toJson(out);
        }

        private void handleResponse(
                final HttpExchange httpExchange,
                final String response) throws Exception {
            OutputStream outputStream = httpExchange.getResponseBody();
            httpExchange.sendResponseHeaders(
                    RESPONSE_HEADERS_CODE, response.length());
            outputStream.write(response.getBytes());
            outputStream.flush();
            outputStream.close();
        }
    }
}
