package statscalc;

public final class Output {
    private final double avX, avY, varX, varY, sdX, sdY, covXY, pearsIndex;

    public Output(
            final double avX,
            final double avY,
            final double varX,
            final double varY,
            final double sdX,
            final double sdY,
            final double covXY,
            final double pearsIndex) {
        super();
        this.avX = avX;
        this.avY = avY;
        this.varX = varX;
        this.varY = varY;
        this.sdX = sdX;
        this.sdY = sdY;
        this.covXY = covXY;
        this.pearsIndex = pearsIndex;
    }

    public double getAvX() {
        return avX;
    }

    public double getAvY() {
        return avY;
    }

    public double getVarX() {
        return varX;
    }

    public double getVarY() {
        return varY;
    }

    public double getSdX() {
        return sdX;
    }

    public double getSdY() {
        return sdY;
    }

    public double getCovXY() {
        return covXY;
    }

    public double getPearsIndex() {
        return pearsIndex;
    }

}
