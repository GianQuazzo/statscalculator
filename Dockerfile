FROM openjdk:8-jdk-slim
ENV TZ=Europe/Rome

RUN apt-get update && apt-get install -y curl

WORKDIR /srv/app/
COPY ./target/*-jar-with-dependencies.jar ./app.jar

EXPOSE 8080
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "app.jar"]
HEALTHCHECK --interval=1m --timeout=3s CMD curl -d "{"x":[0,375,668,5,6],"y":[0,375,668,5,6],"dim":5}" -X POST http://localhost:8080/statsCalculator || exit 1